#!/bin/bash

ansible-playbook install.yml \
    -i inventories/prod/hosts \
    -c local \
    "$@"